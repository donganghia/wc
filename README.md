# README #

### Laravel ### 

* Organize ﬁles and code 
* Rapid application development 
* MVC architecture (and PHP7) 
* Unit testing (FAST on HHVM) 
* Best documentation of any 
* High level of abstraction 
* Overloading capabilities using dynamic methods 
* Tons of out of the box functionality 
* payment integration with stripe 
* very strong encryption packages 
* ORM

### React ###

* Language : Javascript/ECMAScript2015(ES6)/SCMAScript7 (Babel)
* View : React
* Flux Library: Redux
* Css : SCSS, Bootstrap3
* Build : webpack
* TaskRunner : gulp, npm

###  React Native ###

* Build native mobile apps using JavaScript and React.

###  NativeBase ###

* Open Source Framework.
* Build high-quality mobile apps using React Native iOS and Android apps with a fusion of ES6 (ECMAScript)
* NativeBase builds a layer on top of React Native

### Time Wizard ###
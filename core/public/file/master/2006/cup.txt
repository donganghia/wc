##########################################
# World Cup 2006 Germany / Deutschland

Group A  | Germany      Ecuador       Poland          Costa-Rica
Group B  | England      Sweden        Paraguay        Trinidad-Tobago
Group C  | Argentina    Netherlands   Ivory-Coast     Serbia-Montenegro
Group D  | Portugal     Mexico        Angola          Iran
Group E  | Italy        Ghana         Czech-Republic  United-States
Group F  | Brazil       Australia     Croatia         Japan
Group G  | Switzerland  France        South-Korea     Togo
Group H  | Spain        Ukraine       Tunisia         Saudi-Arabia


Matchday 1  | Fri Jun/9
Matchday 2  | Sat Jun/10
Matchday 3  | Sun Jun/11
Matchday 4  | Mon Jun/12
Matchday 5  | Tue Jun/13
Matchday 6  | Wed Jun/14
Matchday 7  | Thu Jun/15
Matchday 8  | Fri Jun/16
Matchday 9  | Sat Jun/17
Matchday 10 | Sun Jun/18
Matchday 11 | Mon Jun/19
Matchday 12 | Tue Jun/20
Matchday 13 | Wed Jun/21
Matchday 14 | Thu Jun/22
Matchday 15 | Fri Jun/23



Group A:

(1)  Fri Jun/9     Germany     4-2 (2-1)  Costa-Rica   @ Allianz Arena, München
(2)  Fri Jun/9     Poland      0-2 (0-1)  Ecuador      @ Veltins-Arena, Gelsenkirchen

(17)  Wed Jun/14   Germany     1-0 (0-0)  Poland       @ Signal Iduna Park, Dortmund

(18)  Thu Jun/15   Ecuador     3-0 (1-0)  Costa-Rica   @ AOL Arena, Hamburg

(33)  Tue Jun/20   Ecuador     0-3 (0-2)  Germany      @ Olympiastadion, Berlin
(34)  Tue Jun/20   Costa-Rica  1-2 (1-1)  Poland       @ AWD-Arena, Hannover


Group B:

(3)  Sat Jun/10    England  1-0 (1-0)  Paraguay          @ Commerzbank-Arena, Frankfurt
(4)  Sat Jun/10    Trinidad-Tobago  0-0  Sweden      @ Signal Iduna Park, Dortmund

(19)  Thu Jun/15   England   2-0 (0-0)  Trinidad-Tobago  @ Frankenstadion, Nürnberg
(20)  Thu Jun/15   Sweden    1-0 (0-0)  Paraguay             @ Olympiastadion, Berlin

(35)  Tue Jun/20   Sweden    2-2 (0-1)  England               @ RheinEnergieStadion, Köln
(36)  Tue Jun/20   Paraguay  2-0 (1-0)  Trinidad-Tobago   @ Fritz-Walter-Stadion, Kaiserslautern


Group C:

(5)  Sat Jun/10    Argentina  2-1 (2-0)  Ivory-Coast         @ AOL Arena, Hamburg

(6)  Sun Jun/11    Serbia-Montenegro  0-1 (0-1)  Netherlands  @ Zentralstadion, Leipzig

(21)  Fri Jun/16   Argentina  6-0 (3-0)  Serbia-Montenegro     @ Veltins-Arena, Gelsenkirchen
(22)  Fri Jun/16   Netherlands  2-1 (2-1)  Ivory-Coast           @ Gottlieb-Daimler-Stadion, Stuttgart

(37)  Wed Jun/21   Netherlands  0-0  Argentina                       @ Commerzbank-Arena, Frankfurt
(38)  Wed Jun/21   Ivory-Coast  3-2 (1-2)  Serbia-Montenegro   @ Allianz Arena, München


Group D:

(7)  Sun Jun/11     Mexico    3-1 (1-1)  Iran        @ Frankenstadion, Nürnberg
(8)  Sun Jun/11     Angola    0-1 (0-1)  Portugal    @ RheinEnergieStadion, Köln

(23)  Fri Jun/16    Mexico    0-0        Angola      @ AWD-Arena, Hannover

(24)  Sat Jun/17    Portugal  2-0 (0-0)  Iran        @ Commerzbank-Arena, Frankfurt

(39)  Wed Jun/21    Portugal  2-1 (2-1)  Mexico      @ Veltins-Arena, Gelsenkirchen
(40)  Wed Jun/21    Iran      1-1 (0-0)  Angola      @ Zentralstadion, Leipzig


Group E:

(9)  Mon Jun/12    Italy           2-0 (1-0)  Ghana           @ AWD-Arena, Hannover
(10) Mon Jun/12    United-States   0-3 (0-2)  Czech-Republic  @ Veltins-Arena, Gelsenkirchen

(25) Sat Jun/17    Italy           1-1 (1-1)  United-States    @ Fritz-Walter-Stadion, Kaiserslautern
(26) Sat Jun/17    Czech-Republic  0-2 (0-1)  Ghana            @ RheinEnergieStadion, Köln

(41) Thu Jun/22    Czech-Republic  0-2 (0-1)  Italy            @ AOL Arena, Hamburg
(42) Thu Jun/22    Ghana           2-1 (2-1)  United-States    @ Frankenstadion, Nürnberg


Group F:

(11)  Tue Jun/13    Brazil     1-0 (1-0)  Croatia       @ Olympiastadion, Berlin

(12)  Mon Jun/12    Australia  3-1 (0-1)  Japan         @ Fritz-Walter-Stadion, Kaiserslautern

(27)  Sun Jun/18    Brazil     2-0 (0-0)  Australia     @ Allianz Arena, München
(28)  Sun Jun/18    Japan      0-0        Croatia       @ Frankenstadion, Nürnberg

(43)  Thu Jun/22    Japan      1-4 (1-1)  Brazil        @ Signal Iduna Park, Dortmund
(44)  Thu Jun/22    Croatia    2-2 (1-1)  Australia     @ Gottlieb-Daimler-Stadion, Stuttgart


Group G:

(13)  Tue Jun/13    France          0-0        Switzerland     @ Gottlieb-Daimler-Stadion, Stuttgart
(14)  Tue Jun/13    South-Korea     2-1 (0-1)  Togo            @ Commerzbank-Arena, Frankfurt

(29)  Sun Jun/18    France          1-1 (1-0)  South-Korea     @ Zentralstadion, Leipzig

(30)  Mon Jun/19    Togo            0-2 (0-1)  Switzerland     @ Signal Iduna Park, Dortmund

(45)  Fri Jun/23    Togo            0-2 (0-0)  France          @ RheinEnergieStadion, Köln
(46)  Fri Jun/23    Switzerland     2-0 (1-0)  South-Korea     @ AWD-Arena, Hannover


Group H:

(15)  Wed Jun/14    Spain         4-0 (2-0)  Ukraine         @ Zentralstadion, Leipzig
(16)  Wed Jun/14    Tunisia       2-2 (1-0)  Saudi-Arabia    @ Allianz Arena, München

(31)  Mon Jun/19    Spain         3-1 (0-1)  Tunisia        @ Gottlieb-Daimler-Stadion, Stuttgart
(32)  Mon Jun/19    Saudi-Arabia  0-4 (0-2)  Ukraine        @ AOL Arena, Hamburg

(47)  Fri Jun/23    Saudi-Arabia  0-1 (0-1)  Spain         @ Fritz-Walter-Stadion, Kaiserslautern
(48)  Fri Jun/23    Ukraine       1-0 (0-0)  Tunisia       @ Olympiastadion, Berlin

Round of 16

(49)  Sat Jun/24    Germany      2-0 (2-0)  Sweden          @ Allianz Arena, München
(50)  Sat Jun/24    Argentina    2-1 a.e.t. (1-1, 1-1)  Mexico   @ Zentralstadion, Leipzig
(51)  Sun Jun/25    England      1-0 (0-0)  Ecuador         @ Gottlieb-Daimler-Stadion, Stuttgart
(52)  Sun Jun/25    Portugal     1-0 (1-0)  Netherlands     @ Frankenstadion, Nürnberg
(53)  Mon Jun/26    Italy        1-0 (0-0)  Australia       @ Fritz-Walter-Stadion, Kaiserslautern
(54)  Mon Jun/26    Switzerland  0-3 pen. 0-0 aet 0-0   Ukraine  @ RheinEnergieStadion, Köln
(55)  Tue Jun/27    Brazil       3-0 (2-0)  Ghana           @ Signal Iduna Park, Dortmund
(56)  Tue Jun/27    Spain        1-3 (1-1)  France          @ AWD-Arena, Hannover


Quarter-finals

(57)  Fri Jun/30    Germany  4-2 pen. 1-1 aet  Argentina   @ Olympiastadion, Berlin
(58)  Fri Jun/30    Italy    3-0 (1-0)  Ukraine                  @ AOL Arena, Hamburg
(59)  Sat Jul/1     England   1-3 pen. 0-0 aet 0-0  Portugal          @ Veltins-Arena, Gelsenkirchen
(60)  Sat Jul/1     Brazil    0-1 (0-0)  France                  @ Commerzbank-Arena, Frankfurt


Semi-finals

(61)  Tue Jul/4    Germany   0-2 a.e.t.  0-0  Italy              @ Signal Iduna Park, Dortmund
                     # -; Grosso 119' Del Piero 120+1'

(62)  Wed Jul/5    Portugal  0-1 (0-1)  France         @ Allianz Arena, München
                     # -; Zidane 33' (pen.)

Match for third place

(63)  Sat Jul/8    Germany  3-1 (0-0)  Portugal         @ Gottlieb-Daimler-Stadion, Stuttgart
                     # Schweinsteiger 56', 78' Petit 60' (o.g.);  Nuno Gomes 88'


Final

(64)  Sun Jul/9    Italy  5-3 pen. 1-1 aet (1-1)   France      @ Olympiastadion, Berlin
                     # Materazzi 19'; Zidane 7' (pen.)




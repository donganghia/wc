{{--@extends('admin.layouts.default')--}}

{{-- Web site Title --}}
{{--@section('title') {{{ trans("admin/daily.header") }}} :: @parent--}}
{{--@stop--}}

{{-- Content --}}
{{--@section('main')--}}
        <!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Shop Homepage</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo Request::root(); ?>/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo Request::root(); ?>/css/shop-homepage.css" rel="stylesheet">

</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="#">
            MAJI SHOP
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">About</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Services</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Contact</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<!-- Page Content -->
<div class="container">

    <div class="row">

        <div class="col-lg-3">

            <h1 class="my-4" style="padding-top: 20px;">
                <img height="70" src="<?php echo Request::root(); ?>/image/ui/shop/maji.png" >
            </h1>
            <!-- Search Widget -->
            <div class="card my-4">
                <h5 class="card-header">Search</h5>
                <div class="card-body">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                        <span class="input-group-btn">
                  <button class="btn btn-secondary" type="button">Go!</button>
                </span>
                    </div>
                </div>
            </div>
            <div class="list-group">
                <?php
                 foreach ($category as $cate)
                 {
                 ?>
                    <a href="<?php echo URL::to('ui/shop/' . $cate->id . '/index'); ?>" class="list-group-item"><?php echo $cate->name;?></a>
                 <?php
                 }
                 ?>
            </div>

        </div>
        <!-- /.col-lg-3 -->

        <div class="col-lg-9">

            <div id="carouselExampleIndicators" class="carousel slide my-4" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active">
                        <img class="d-block img-fluid" src="<?php echo Request::root(); ?>/image/ui/shop/sk2.png" alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block img-fluid" src="<?php echo Request::root(); ?>/image/ui/shop/muji.png" alt="Second slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block img-fluid" src="<?php echo Request::root(); ?>/image/ui/shop/sekkisei.png" alt="Third slide">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>

            <div class="row">
            <?php
                $stars = [
                    "3" =>  "★ ★ ★",
                    "4" =>  "★ ★ ★ ★",
                    "5" =>  "★ ★ ★ ★ ★",
                ] ;

                foreach ($products as $product)
                {
            ?>
                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="card h-100">
                        <a href="#"><img class="card-img-top" src="<?php echo Request::root(); ?>/image/ui/shop/<?php echo $product->image;?>" alt=""></a>
                        <div class="card-body">
                            <h4 class="card-title">
                                <a href="#"><?php echo $product->name;?></a>
                            </h4>
                            <h5><?php echo $product->price;?></h5>
                            <p class="card-text"><?php echo $product->description;?></p>
                        </div>
                        <div class="card-footer">
                            <small class="text-muted"><?php echo $stars[$product->vote];?></small>
                        </div>
                    </div>
                </div>
            <?php
                }
            ?>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.col-lg-9 -->

    </div>
    <!-- /.row -->

</div>
<!-- /.container -->

<!-- Footer -->
<footer class="py-5 bg-dark">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Maj Maj 2018</p>
    </div>
    <!-- /.container -->
</footer>

<!-- Bootstrap core JavaScript -->
<script src="<?php echo Request::root(); ?>/js/jquery.min.js"></script>
<script src="<?php echo Request::root(); ?>/js/bootstrap.bundle.min.js"></script>

</body>

</html>


{{--@stop--}}

{{-- Scripts --}}
{{--@section('scripts')--}}
    {{--@parent--}}
    {{--<script type="text/javascript">--}}
    {{--</script>--}}
{{--@stop--}}



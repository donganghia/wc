<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeamPlayerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);
        if (!Schema::hasTable('team_player'))
        {
            Schema::create('team_players', function (Blueprint $table)
            {
                $table->increments('id');
                $table->smallInteger('player_id', false, true);
                $table->smallInteger('team_id', false, true);
                $table->tinyInteger('age', false, true);
                $table->string('position', 64);
                $table->tinyInteger('number', false, true);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('team_player');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorldcupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);
        if (!Schema::hasTable('worldcup'))
        {
            Schema::create('worldcup', function (Blueprint $table)
            {
                $table->increments('id');
                $table->smallInteger('national_id', false, true);
                $table->smallInteger('year', false, true);
                $table->string('avatar')->nullable();
                $table->string('description')->nullable();
                $table->date('start_date')->nullable();
                $table->date('end_date')->nullable();
                $table->smallInteger('enabled', false, true)->default(1);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('worldcup');
    }
}

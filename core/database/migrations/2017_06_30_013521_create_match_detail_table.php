<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatchDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);
        if (!Schema::hasTable('match_detail'))
        {
            Schema::create('match_detail', function (Blueprint $table)
            {
                $table->increments('id');
                $table->smallInteger('match_id', false, true);
                $table->string('type', 32);
                $table->smallInteger('team_player_id', false, true);
                $table->string('time', 128);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('match_detail');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatchTeamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);
        if (!Schema::hasTable('match_team'))
        {
            Schema::create('match_team', function (Blueprint $table)
            {
                $table->increments('id');
                $table->smallInteger('match_id', false, true);
                $table->smallInteger('team_id', false, true);
                $table->tinyInteger('goal', false, true);
                $table->tinyInteger('lose', false, true);
                $table->tinyInteger('point', false, true);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('match_team');
    }
}

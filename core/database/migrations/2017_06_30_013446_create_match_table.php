<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);
        if (!Schema::hasTable('match'))
        {
            Schema::create('match', function (Blueprint $table)
            {
                $table->increments('id');
                $table->integer('worldcup_id', false, true);
                $table->dateTime('datetime');
                $table->string('stadium');
                $table->string('round', 64);
                $table->string('extend', 64);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('match');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);
        if (!Schema::hasTable('team'))
        {
            Schema::create('team', function (Blueprint $table)
            {
                $table->increments('id');
                $table->tinyInteger('group_id', false, true);
                $table->smallInteger('national_id', false, true);
                $table->tinyInteger('worldcup_id', false, true);
                $table->string('name', 64);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('team');
    }
}

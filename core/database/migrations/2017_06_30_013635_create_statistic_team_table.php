<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatisticTeamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);
        if (!Schema::hasTable('statistic_team'))
        {
            Schema::create('statistic_team', function (Blueprint $table)
            {
                $table->increments('id');
                $table->smallInteger('national_id', false, true);
                $table->smallInteger('appearance', false, true);
                $table->tinyInteger('winners', false, true);
                $table->tinyInteger('runners_up', false, true);
                $table->tinyInteger('third', false, true);
                $table->tinyInteger('fourth', false, true);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('statistic_team');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatchSquadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);
        if (!Schema::hasTable('match_squad'))
        {
            Schema::create('match_squads', function (Blueprint $table)
            {
                $table->increments('id');
                $table->smallInteger('match_id', false, true);
                $table->smallInteger('team_player_id', false, true);
                $table->string('position_in_match', 64);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('match_squad');
    }
}

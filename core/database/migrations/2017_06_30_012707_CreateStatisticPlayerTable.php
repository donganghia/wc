<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatisticPlayerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);
        if (!Schema::hasTable('statistic_player'))
        {
            Schema::create('statistic_players', function (Blueprint $table)
            {
                $table->increments('id');
                $table->smallInteger('player_id', false, true);
                $table->smallInteger('goals', false, true);
                //$table->foreign('player_id')->references('id')->on('player');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('statistic_player');
    }
}


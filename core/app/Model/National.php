<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class National extends Model
{
    protected $table = 'national';
    public $timestamps = false;
}

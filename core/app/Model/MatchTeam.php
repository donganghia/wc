<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MatchTeam extends Model
{
    protected $table = 'match_team';
    public $timestamps = false;

    public function team()
    {
        return $this->belongsTo('App\Model\Team', 'team_id', 'id');
    }
}

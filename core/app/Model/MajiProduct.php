<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MajiProduct extends Model
{
    protected $table = 'maji_product';
    public $timestamps = false;

    public function majiCategory()
    {
        return $this->belongsTo('App\Model\MajiCategory', 'category_id', 'id');
    }

    public function majiSkill()
    {
        return $this->belongsTo('App\Model\MajiSkill', 'skill_id', 'id');
    }

    public function majiBrand()
    {
        return $this->belongsTo('App\Model\MajiBrand', 'brand_id', 'id');
    }
}

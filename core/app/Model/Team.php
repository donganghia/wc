<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $table = 'team';
    public $timestamps = false;

    public function group()
    {
        return $this->belongsTo('App\Model\Group', 'group_id', 'id');
    }

    public function national()
    {
        return $this->belongsTo('App\Model\National', 'national_id', 'id');
    }

    public function worldcup()
    {
        return $this->belongsTo('App\Model\Worldcup', 'worldcup_id', 'id');
    }

    public function teamPlayer()
    {
        return $this->hasMany('App\Model\TeamPlayer', 'team_id', 'id');
    }

    public function matchTeam()
    {
        return $this->hasMany('App\Model\MatchTeam', 'team_id', 'id');
    }
}

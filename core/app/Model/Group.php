<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $table = 'group';
    public $timestamps = false;

    public function team()
    {
        return $this->hasMany('App\Model\Team', 'group_id', 'id');
    }
}

<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Match extends Model
{
    protected $table = 'match';
    public $timestamps = false;

    public function matchTeams()
    {
        return $this->hasMany('App\Model\MatchTeam', 'match_id', 'id');
    }
}

<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Worldcup extends Model
{
    protected $table = 'worldcup';
    public $timestamps = false;

    public function national()
    {
        return $this->belongsTo('App\Model\National', 'national_id', 'id');
    }

    public function teams()
    {
        return $this->hasMany('App\Model\Team', 'worldcup_id', 'id');
    }

    public function matches()
    {
        return $this->hasMany('App\Model\Match', 'worldcup_id', 'id');
    }
}

<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MajiCategory extends Model
{
    protected $table = 'maji_category';
    public $timestamps = false;
}

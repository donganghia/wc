<?php
/**
 * Created by PhpStorm.
 * User: ntran
 * Date: 2017/07/20
 * Time: 11:48
 */

namespace App\Response;


abstract class ResponseBase
{
    public function __construct($mainClass)
    {
        $this->setValue($this, $mainClass);
    }

    public function setValue(&$target, $source)
    {
        foreach ($source as $key => $value)
        {
            if (property_exists($target, $key))
            {
                $target->$key = $value;
            }
        }
    }
}
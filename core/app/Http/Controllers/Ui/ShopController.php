<?php

namespace App\Http\Controllers\Ui;

use App\Http\Controllers\Controller;
use App\Model\MajiProduct;
use App\Model\MajiCategory;
use Illuminate\Http\Request;

class ShopController extends Controller
{

    public function index($params = [])
    {
        if(isset($params['categoryId']))
        {
            $products = MajiProduct::select('id', 'name', 'description', 'price', 'vote', 'image')
                ->where("group_by","=",1)
                ->where("category_id","=",$params['categoryId'])
                ->orderBy("order_by")->get();
        }
        else
        {
            $products = MajiProduct::select('id', 'name', 'description', 'price', 'vote', 'image')
                ->where("group_by","=",1)
                ->orderBy("order_by")->get();
        }

        $category = MajiCategory::select('id', 'name')
            ->where('delete_flag','=',0)
            ->orderBy("order_by")->get();

        return view('ui.shop.index', compact('category', 'products'));
    }

    public  function getIndex($categoryId)
    {
        return $this->index(['categoryId' => $categoryId]);
    }
}

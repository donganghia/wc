<?php

namespace App\Http\Controllers\Data;

use App\Http\Controllers\Controller;
use App\Model\Group;
use App\Model\Match;
use App\Model\MatchTeam;
use App\Model\National;
use App\Model\Team;
use App\Model\Worldcup;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;

class ConvertController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $filesystem = new Filesystem();
        $directories = $filesystem->directories(public_path() . "/file/master");

        foreach ($directories as $directory) {
            $files = $filesystem->files($directory);
            foreach ($files as $file) {
                $year = self::getYear($directory);
                $data = explode("\n", $filesystem->get($file));

                switch (self::format($file)) {
                    case "cup":
                        self::matchCup($data, $year);
                        break;
                    case "cup_finals":
                    default:
                        break;
                }
            }
        }
    }

    public function getYear($file)
    {
        return substr(pathinfo($file, PATHINFO_FILENAME), -4, 4);
    }

    public function format($file)
    {
        return pathinfo($file, PATHINFO_FILENAME);
    }

    public function matchCup($data, $year)
    {
        $allMonth = ["05" => "May", "06" => "June", "07" => "July", "7" => "Jul", "6" => "Jun"];
        $allDateInMonth = range(1, 31);
        $allDateInWeek = ["Mon","Sat","Sun","Thu","Tue","Fri","Wed"];
        $allTeam = [];
        $round = "0";
        // loop line
        foreach ($data as $i => $val) {
            if ($i == 0 || trim($val) == "") {
                continue;
            }

            $round = self::roundType($val) ?: $round;
            $val = str_replace("–", "-", $val);
            // Worldcup  (line 1)
            if ($i == 1) {
                $host = explode(" ", $val);
                $wc = Worldcup::where('year', '=', $host[3])->first() ?: new Worldcup();
                $wc->year = $host[3];

                $national = self::getNational(trim(str_replace("-", " ", $host[4])));
                $wc->national_id = $national->id;

                $wc->save();
                continue;
            }

            //Group &&  Team ("Group" && "|")
            if (strpos($val, "|") !== false && strpos($val, "Group") !== false) {
                list($groupName, $teamName) = explode(" | ", $val);

                //save group
                $group = Group::where('name', '=', $groupName)->first() ?: new Group();
                $group->name = $groupName;
                $group->save();

                $teams = explode(" ", $teamName);

                foreach ($teams as $val) {
                    if ($val == "") {
                        continue;
                    }

                    $val = trim(str_replace("-", " ", $val));
                    $national = self::getNational($val);

                    //save team
                    //$team = Team::where('worldcup_id', '=', $wc->id)->where('name', '=', $val)->first() ?: new Team();
                    $team              = new Team();
                    $team->name = $val;
                    $team->group_id = $group->id;
                    $team->national_id = $national->id;
                    $team->worldcup_id = $wc->id;
                    $team->save();

                    $allTeam[$team->id] = $val;
                }

            }

            //"@"
            if (strpos($val, "@") !== false) {
                list($matchInfo, $stadium) = explode("@", $val);
                $matchAry = explode(" ", $matchInfo);
                $date = "";
                $month = "";
                $teamid = [];
                $score = [];
                $lose = [];
                $point = [];
                $extend = "";
                $penResult = [];
                $position = 0;
                foreach ($matchAry as $key => $val) {
                    if (trim($val) === "") {
                        continue;
                    }
                    $position++;

                    if ($position === 2 && in_array($val, $allDateInMonth)) {
                        $date = $val;
                    }
                    elseif($position === 2 && in_array($val, $allDateInWeek))
                    {
                        $t = explode("/",$matchAry[$key+1]);

                        $date = $t[1];
                        $month = array_search($t[0], $allMonth);
                    }

                    if (in_array($val, $allMonth)) {
                        $month = array_search($val, $allMonth);
                    }

                    if (in_array(str_replace("-", " ", $val), $allTeam)) {
                        $teamid[] = array_search(str_replace("-", " ", $val), array_reverse($allTeam, true));
                    }

                    if (empty($score) && strlen($val) < 6 && strpos($val, "-") !== false && strpos($val, "-;") === false && strpos($val, ")") === false) {
                        if ($matchAry[$key + 1] === "pen.") {
                            $extend = $val . " pen.";

                            $tmp = explode("-", $val);
                            $penResult[] = $tmp[0] > $tmp[1] ? 3 : 0;
                            $penResult[] = $tmp[0] > $tmp[1] ? 0 : 3;

                            $val = $matchAry[$key + 2];
                        }

                        $score = explode("-", $val);

                        if ($matchAry[$key + 1] === "a.e.t." && $extend === "") {
                            $extend = "a.e.t.";
                            if($score[0] == $score[1] )
                            {
                                $penResult[] = 3;
                                $penResult[] = 0;
                            }
                        }

                        $lose = array_reverse($score);
                        $point[] = $score[0] == $score[1] ? (!empty($penResult) ? $penResult[0] : 1) : ($score[0] > $score[1] ? 3 : 0);
                        $point[] = $score[0] == $score[1] ? (!empty($penResult) ? $penResult[1] : 1) : ($score[0] < $score[1] ? 3 : 0);
                    }
                }

                //Match
                $match = new Match();
                $match->datetime = $year . "-" . $month . "-" . $date;
                $match->worldcup_id = $wc->id;
                $match->stadium = $stadium;
                $match->round = $round;
                $match->extend = $extend;
                $match->save();

                //MatchTeam
                for ($i = 0; $i < 2; $i++) {
                    $matchTeam = new MatchTeam();
                    $matchTeam->match_id = $match->id;
                    $matchTeam->team_id = $teamid[$i];
                    $matchTeam->goal = $score[$i];
                    $matchTeam->lose = $lose[$i];
                    $matchTeam->point = $point[$i];
                    $matchTeam->save();
                }
            }
        }
    }

    public function getNational($val)
    {" => "Germany", "Soviet Union" => "Russia", "Dutch East Indies" => "Indonesia"];
        $nationalName = array_key_exists($val, $special) ? $special[$val] : $val;
        //save national
        $special = ["West Germany" => "Germany", "East Germany
        $national = National::where('name', '=', $nationalName)->first();
        if (!$national) {
            $national = new National();
            $national->name = $nationalName;
            $national->save();
        }

        return $national;
    }

    public function roundType($line)
    {
        $roundType = [
            "01" => "1st Qualifying",
            "02" => "2nd Qualifying",
            "03" => "Round of 16",
            "04" => "Quarter-finals",
            "05" => "Semi-finals",
            "06" => "Match for third place",
            "07" => "Final",
            "08" => "Preliminary round",
            "09" => "First round",
            "10" => "First round replays",
            "11" => "Final Qualifying",
        ];

        if (strpos($line, "Group I") !== false || strpos($line, "Group J") !== false) {
            return "02";
        } else if (strpos($line, "Group") !== false) {
            return "01";
        } else {
            foreach ($roundType as $key => $value) {
                if (strpos($line, $value) !== false) {
                    return $key;
                }
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

}

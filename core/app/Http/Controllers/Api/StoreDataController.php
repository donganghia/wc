<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\Match;
use App\Model\National;
use App\Model\Worldcup;
use Illuminate\Http\Request;

class StoreDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }


    public function statistic()
    {
        $worldcups = Worldcup::where('enabled', '=', 1)->get();

        $data = [];
        foreach ($worldcups as $worldcup) {
            $endMatch = $worldcup->matches->last();

            $matchTeams = $endMatch->matchTeams;

            foreach ($matchTeams as $matchTeam) {
                //                    $wc->champion = $worldcup->matches->last()->matchTeams->where('point', 3)->first()->team->national->name;
                if (!isset($data[$matchTeam->team->national->id])) {
                    $data[$matchTeam->team->national->id]["id"] = $matchTeam->team->national->id;
                    $data[$matchTeam->team->national->id]["name"] = $matchTeam->team->national->name;
                    $data[$matchTeam->team->national->id]["avatar"] = $matchTeam->team->national->avatar;
                    $data[$matchTeam->team->national->id]["winners"] = 0;
                    $data[$matchTeam->team->national->id]["runnersUp"] = 0;
                    $data[$matchTeam->team->national->id]["third"] = 0;
                    $data[$matchTeam->team->national->id]["fourth"] = 0;
                }
                if ((int)$matchTeam->point === 3) {
                    $data[$matchTeam->team->national->id]["winners"]++;
                    $data[$matchTeam->team->national->id]["winnersYears"][] = $worldcup->year;
                } else {
                    $data[$matchTeam->team->national->id]["runnersUp"]++;
                    $data[$matchTeam->team->national->id]["runnersUpYears"][] = $worldcup->year;
                }

            }

            //third
            $thirdMatch = Match::where('round', '=', '06')->where('worldcup_id', '=', $worldcup->id)->get();
            if (!empty($thirdMatch->toArray())) {

                $matchTeams = $thirdMatch->first()->matchTeams;

                foreach ($matchTeams as $matchTeam) {
                    if (!isset($data[$matchTeam->team->national->id])) {
                        $data[$matchTeam->team->national->id]["id"] = $matchTeam->team->national->id;
                        $data[$matchTeam->team->national->id]["name"] = $matchTeam->team->national->name;
                        $data[$matchTeam->team->national->id]["avatar"] = $matchTeam->team->national->avatar;
                        $data[$matchTeam->team->national->id]["winners"] = 0;
                        $data[$matchTeam->team->national->id]["runnersUp"] = 0;
                        $data[$matchTeam->team->national->id]["third"] = 0;
                        $data[$matchTeam->team->national->id]["fourth"] = 0;
                    }
                    if ((int)$matchTeam->point === 3) {
                        $data[$matchTeam->team->national->id]["third"]++;
                    } else {
                        $data[$matchTeam->team->national->id]["fourth"]++;
                    }
                }
            } else {
                if ($worldcup->year == 1930) {
                    $data[11] = [
                        "id" => 11,
                        "name" => 'United States',
                        "avatar" => 'US',
                        "winners" => 0,
                        "runnersUp" => 0,
                        "third" => 1,
                        "fourth" => 0,
                    ];
                    $data[6] = [
                        "id" => 6,
                        "name" => 'Yugoslavia',
                        "avatar" => 'CS',
                        "winners" => 0,
                        "runnersUp" => 0,
                        "third" => 0,
                        "fourth" => 1,
                    ];
                }

                if ($worldcup->year == 1950) {
                    $data[15]["third"] = 1;
                    $data[18] = [
                        "id" => 18,
                        "name" => 'Spain',
                        "avatar" => 'ES',
                        "winners" => 0,
                        "runnersUp" => 0,
                        "third" => 0,
                        "fourth" => 1,
                    ];
                }
            }
        }

        $statistic = $data ? array_values($data) : [];
        usort($statistic, function ($a, $b) {
            if ($a["winners"] === $b["winners"]) {
                if ($a["runnersUp"] === $b["runnersUp"]) {
                    if ($a["third"] == $b["third"]) {
                        return ($a["fourth"] < $b["fourth"]) ? 1 : -1;
                    } else {
                        return ($a["third"] < $b["third"]) ? 1 : -1;
                    }
                } else {
                    return ($a["runnersUp"] < $b["runnersUp"]) ? 1 : -1;
                }
            }

            return ($a["winners"] < $b["winners"]) ? 1 : -1;
        });

        return !empty($statistic) ? response()->json(["data" => $statistic], 200)
            : response()->json(["message" => "no data available"], 404);
    }

    public function compare()
    {
        //get national
        $national = National::select('id', 'name as value', 'avatar')
            ->where("name","<>","")->where("name","<>","Korea Japan")->orderBy("name")->get()->toArray();

        //vs
        $result = [];
        $matches = Match::select('id')->get();

        foreach ($matches as $match) {
            $matchTeams = $match->matchTeams()->get();
            $tmp = [];
            foreach ($matchTeams as $matchTeam) {
                $tmp[] = $matchTeam;
            }

            $n0 = $tmp[0]->team->national->id;
            $n1 = $tmp[1]->team->national->id;

            // ----
            $k01 = $n0 . '-' . $n1;
            if (!isset($result[$k01])) {
                $result[$k01] = ['mapping' => $k01, 'match' => 0, 'win' => 0, 'draw' => 0, 'lose' => 0, 'goal' => 0, 'goalLose' => 0];
            }

            $result[$k01]['match'] += 1;
            $result[$k01]['goal'] += $tmp[0]->goal;
            $result[$k01]['goalLose'] += $tmp[0]->lose;
            switch ($tmp[0]->point) {
                case 0:
                    $result[$k01]['lose'] += 1;
                    break;
                case 1:
                    $result[$k01]['draw'] += 1;
                    break;
                case 3:
                    $result[$k01]['win'] += 1;
                    break;
            }

            // ----
            $k10 = $n1 . '-' . $n0;
            if (!isset($result[$k10])) {
                $result[$k10] = ['mapping' => $k10, 'match' => 0, 'win' => 0, 'draw' => 0, 'lose' => 0, 'goal' => 0, 'goalLose' => 0];
            }
            $result[$k10]['match'] += 1;
            $result[$k10]['goal'] += $tmp[1]->goal;
            $result[$k10]['goalLose'] += $tmp[1]->lose;
            switch ($tmp[1]->point) {
                case 0:
                    $result[$k10]['lose'] += 1;
                    break;
                case 1:
                    $result[$k10]['draw'] += 1;
                    break;
                case 3:
                    $result[$k10]['win'] += 1;
                    break;
            }

        }

        return !empty($national) ? response()->json(["data" => ["national" => $national, "result" => array_values($result)]], 200)
            : response()->json(["message" => "no data available"], 404);
    }

    public function statisticByPlayer()
    {
        //
    }

    public function statisticByNational()
    {
        //
    }

    public function getWorldcupInfo(Request $request)
    {
        $worldcupId = $request->input("post")["conditions"]["id"];

        $worldcups = $worldcupId
            ? Worldcup::where('id', '=', $worldcupId)->where('enabled', '=', 1)->orderBy("year", "desc")->get()
            : Worldcup::where('enabled', '=', 1)->orderBy("year", "desc")->get();

        if (!empty($worldcups)) {
            $data = [];
            foreach ($worldcups as $worldcup) {
                //if(in_array($worldcup->id,[2, 3])) continue;

                $tmp = $this->getExcerpt($worldcup, ["id", "year", "start_date", "end_date"]);
                $tmp->host = $worldcup->national->name;
                $tmp->info = $this->getInfo($worldcup);

                $data[] = $tmp;
            }
        }

        return !empty($data) ? response()->json(["data" => $data], 200)
            : response()->json(["message" => "no data available"], 404);
    }

    public function getExcerpt($obj, $keys)
    {
        $excerpt = new \stdClass();
        foreach ($keys as $key) {
            $excerpt->$key = $obj->$key;
        }

        return $excerpt;
    }

    public function getInfo($worldcup)
    {
        $roundType = [
            "01" => "1st Qualifying",
            "02" => "2nd Qualifying",
            "03" => "8th Finals",
            "04" => "Quarter-finals",
            "05" => "Semi-finals",
            "06" => "3rd Match",
            "07" => "Final",
            "08" => "Preliminary round",
            "09" => "First round",
            "10" => "First round replays",
            "11" => "Final Qualifying",
        ];

        $rounds = [];
        foreach ($worldcup->matches as $match) {
            $rounds[$match->round][] = $match;
        }

        $jsonKnoukout = [];
        $jsonChart = [];
        $finalStages = [];
        foreach ($rounds as $key => $round) {

            if ($key === "01" || $key === "02" ||
                ($key === "07" && $worldcup->year == 1950)
            ) {
                $groupName = [];
                $jsonRanks = [];
                $jsonMatches = [];
                foreach ($round as $match) {
                    $jsonMatchResult = [];
                    $groupId = null;
                    foreach ($match->matchTeams as $matchTeam) {
                        $team = $matchTeam->team;
                        $groupId = ($key === "07" && $worldcup->year == 1950) ? $key : $team->group->id;

                        $jsonMatchResult[] = [
                            "name" => $team->name,
                            "goal" => $matchTeam->goal,
                            "avatar" => $team->national->avatar,
                        ];

                        if (!isset($jsonRanks[$groupId][$team->id])) {
                            $groupName[$groupId] = $team->group->name;

                            $jsonRanks[$groupId][$team->id] = [
                                "teamId" => $team->id,
                                "teamName" => $team->name,
                                "avatar" => $team->national->avatar,
                                "matches" => 0,
                                "wins" => 0,
                                "draws" => 0,
                                "losses" => 0,
                                "goalsFor" => 0,
                                "goalsAgainst" => 0,
                                "points" => 0,
                            ];
                        }

                        $jsonRanks[$groupId][$team->id]["goalsFor"] += (int)$matchTeam->goal;
                        $jsonRanks[$groupId][$team->id]["goalsAgainst"] += (int)$matchTeam->lose;

                        if ((int)$matchTeam->point === 0) {
                            $jsonRanks[$groupId][$team->id]["losses"] += 1;
                        } elseif ((int)$matchTeam->point === 1) {
                            $jsonRanks[$groupId][$team->id]["draws"] += 1;
                            $jsonRanks[$groupId][$team->id]["points"] += 1;
                        } elseif ((int)$matchTeam->point === 3) {
                            $jsonRanks[$groupId][$team->id]["wins"] += 1;
                            $jsonRanks[$groupId][$team->id]["points"] += 3;
                        }

                        $jsonRanks[$groupId][$team->id]["matches"] += 1;

                    }

                    $jsonMatches[$groupId][] = [
                        "id" => $match->id,
                        "date" => date("d/m/Y", strtotime($match->datetime)),
                        "stadium" => str_replace("\r",'', $match->stadium),
                        "result" => $jsonMatchResult,
                    ];

                }

                foreach ($jsonMatches as $groupId => $jsonMatch) {
                    $ranks = array_values($jsonRanks[$groupId]);
                    usort($ranks, function ($a, $b) {
                        if ($a["points"] === $b["points"]) {
                            $a1 = $a["goalsFor"] - $a["goalsAgainst"];
                            $b1 = $b["goalsFor"] - $b["goalsAgainst"];
                            if ($a1 === $b1) {
                                return ($a["goalsFor"] < $b["goalsFor"]) ? 1 : -1;
                            } else {
                                return ($a1 < $b1) ? 1 : -1;
                            }
                        }

                        return ($a["points"] < $b["points"]) ? 1 : -1;
                    });

                    $jsonChart[] = [
                        "groupId" => $groupId,
                        "groupName" => $groupId === "07" ? "Final Stages" : $groupName[$groupId],
                        "match" => $jsonMatch,
                        "rank" => $ranks,
                    ];
                }
                //} elseif ($key === "07" && $worldcup->year == 1950) {
            } else {
                foreach ($round as $match) {
                    $jsonMatchResult = [];
                    foreach ($match->matchTeams as $matchTeam) {
                        $team = $matchTeam->team;

                        $jsonMatchResult[] = [
                            "name" => $team->name,
                            "goal" => $matchTeam->goal,
                            "avatar" => $team->national->avatar,
                        ];
                    }


                    $jsonKnoukout[$match->round][] = [
                        "id" => $match->id,
                        "date" => date("d/m/Y", strtotime($match->datetime)),
                        "stadium" => str_replace("\r", '', $match->stadium),
                        "extend" => $match->extend,
                        "result" => $jsonMatchResult,
                    ];

                }

                $finalStages = [];
                foreach ($jsonKnoukout as $key => $value) {
                    $finalStages[] = [
                        "stageName" => $roundType[$key],
                        "stageResult" => $value,
                    ];
                }
            }
        }

        $jsonInfo = [
            "chart" => !empty($jsonChart) ? $jsonChart : null,
            "finalStages" => !empty($finalStages) ? $finalStages : null,
        ];

        return $jsonInfo;
    }

    public function getMatchInfo($matchId)
    {
        //
    }

    public function findWorldcup(Request $request)
    {
        $conditions = $request->input("post")["conditions"];

        $data = [];
        if (isset($conditions["enabled"])) {
            $worldcups = Worldcup::where('enabled', '=', $conditions["enabled"])->orderBy("year", "desc")->get(["id", "year", "national_id"]);

            if (!empty($worldcups->toArray())) {
                foreach ($worldcups as $worldcup) {
                    $wc = $this->getExcerpt($worldcup, ["id", "year"]);
                    $wc->host = $worldcup->national->name;
                    $wc->champion = $worldcup->matches->last()->matchTeams->where('point', 3)->first()->team->national->name;
                    $wc->avatar = $worldcup->national->avatar;

                    $data[] = $wc;
                }
            }
        }

        return !empty($data) ? response()->json(["data" => $data], 200)
            : response()->json(["message" => "no data available"], 404);
    }

    public function update(Request $request, $id)
    {
    }

}

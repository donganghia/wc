<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('convert', 'Data\ConvertController', [
    'only' => ['index']
]);

Route::post('store/statistic', 'Api\StoreDataController@statistic');
Route::post('store/compare', 'Api\StoreDataController@compare');
Route::post('store/find', 'Api\StoreDataController@findWorldcup');
Route::post('store/info', 'Api\StoreDataController@getWorldcupInfo');
Route::post('store/match', 'Api\StoreDataController@getMatchInfo');
Route::post('store/national', 'Api\StoreDataController@statisticByNational');
Route::post('store/player', 'Api\StoreDataController@statisticByPlayer');

//Route::get('ui/shop', function () {
//    return view('welcome');
//});
Route::get('ui/shop', 'Ui\ShopController@index');
Route::get('ui/shop/{id}/index', 'Ui\ShopController@getIndex');
//Route::post('category/{id}/edit', 'CategoryController@postEdit');
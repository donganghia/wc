<?php

// ----------------------------- Statistic ----------------------------------
$lotoSeven = [];
$lotoSeven[194] = ['03','06','27','30','31','32','36','14','23'];
$lotoSeven[195] = ['04','07','12','20','23','24','31','26','35'];
$lotoSeven[196] = ['07','13','15','25','30','32','37','08','14'];
$lotoSeven[197] = ['06','08','10','12','16','21','28','14','23'];
$lotoSeven[198] = ['18','21','31','32','33','36','37','05','25'];
$lotoSeven[199] = ['01','09','17','20','22','28','29','13','16'];
$lotoSeven[200] = ['13','14','17','21','27','28','34','01','11'];
$lotoSeven[201] = ['08','11','17','24','28','31','32','12','34'];
$lotoSeven[202] = ['03','07','11','16','22','34','37','29','32'];
$lotoSeven[203] = ['06','13','17','22','32','33','36','12','28'];
$lotoSeven[204] = ['05','19','20','21','30','34','36','07','12'];

$lotoSevenStatistic = [];
foreach($lotoSeven as $sev)
{
	$lotoSevenStatistic = array_merge($lotoSevenStatistic,$sev);
}

$lotoSevenCountValue = array_count_values($lotoSevenStatistic);

// -------------------------------------------------------------------------
echo "Total: ".count($lotoSeven).PHP_EOL;
foreach ($lotoSevenCountValue as $key => $value)
{
	echo $key ."--->".$value."   ";
}
echo PHP_EOL;

echo "Not exist: ";
$lotoSevenUnique = array_unique($lotoSevenStatistic);

$output = [];
$i = 0;
foreach(range(1,37) as $v)
{
	//if(!in_array($v,$lotoSevenUnique)) echo sprintf("%'.02d", $v )." ";
	if(!in_array($v,$lotoSevenUnique)) $output[$i][] = sprintf("%'.02d", $v );
}
echo PHP_EOL;

// ------------------------ Print result -----------------------------------

$numExist = [];
while($i < 3)
{
    $number = sprintf("%'.02d", rand(1,37) );
    if(!in_array($number,$numExist) && isset($lotoSevenCountValue[$number]) &&    $lotoSevenCountValue[$number] < 4  )
    {
		
		$output[$i][] = $number;
        $numExist[] = $number;
		
        if(count($output[$i]) === 7)
        {
			sort($output[$i]);
			$i++;
        }

    }
}
foreach( $output as $v)
{
	echo join(" ",$v).PHP_EOL;
}

// -------------------------------------------------------------------------

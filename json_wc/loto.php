<?php

// ----------------------------- Statistic ----------------------------------
$lotoSix = [];
$lotoSix[1146] = ['01','10','19','23','26','34','27'];
$lotoSix[1147] = ['02','03','10','16','26','27','23'];
$lotoSix[1148] = ['03','05','16','23','31','43','39'];
$lotoSix[1149] = ['02','14','22','23','35','43','31'];
$lotoSix[1150] = ['07','11','17','27','32','40','18'];
$lotoSix[1151] = ['05','23','25','26','29','31','19'];
$lotoSix[1152] = ['06','08','10','33','35','43','22'];
$lotoSix[1153] = ['04','15','20','24','26','30','29'];
$lotoSix[1154] = ['08','12','30','36','41','42','04'];
$lotoSix[1155] = ['14','19','22','23','28','43','25'];
$lotoSix[1156] = ['03','06','19','31','35','40','12'];
$lotoSix[1157] = ['05','17','20','28','39','42','01'];
$lotoSix[1158] = ['22','24','27','30','32','40','23'];
$lotoSix[1159] = ['01','04','15','16','23','30','17'];
$lotoSix[1160] = ['09','22','31','32','34','43','24'];
$lotoSix[1161] = ['01','02','12','16','19','40','18'];
$lotoSix[1162] = ['03','04','08','12','24','37','10'];

$lotoSixStatistic = [];
foreach($lotoSix as $six)
{
	$lotoSixStatistic = array_merge($lotoSixStatistic,$six);
}

$lotoSixCountValue = array_count_values($lotoSixStatistic);

// -------------------------------------------------------------------------
echo "Total: ".count($lotoSix).PHP_EOL;
foreach ($lotoSixCountValue as $key => $value)
{
	echo $key ."--->".$value."   ";
}
echo PHP_EOL;

echo "Not exist: ";
$lotoSixUnique = array_unique($lotoSixStatistic);
foreach(range(1,43) as $v)
{
	if(!in_array($v,$lotoSixUnique)) echo sprintf("%'.02d", $v )." ";
}
echo PHP_EOL;

// ------------------------ Print result -----------------------------------
$i = 0;
$output = [];
$numExist = [];
while($i < 3)
{
    $number = sprintf("%'.02d", rand(1,43) );
    if(!in_array($number,$numExist)  &&  $lotoSixCountValue[$number] < 5  )
    {
        $output[$i][] = $number;
        $numExist[] = $number;
        if(count($output[$i]) === 6)
        {
			sort($output[$i]);
			$i++;
        }
    }
}
foreach( $output as $v)
{
	echo join(" ",$v).PHP_EOL;
}






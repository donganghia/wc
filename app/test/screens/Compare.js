import React from "react";
import { Dropdown } from "react-native-material-dropdown";
import HeaderTab from "../components/HeaderTab";
import AdsAdMob from "../components/AdsAdMob";
import { Badge, Text, View } from "native-base";
import Flag from "react-native-flags";

export default class CompareScreen extends React.Component {
  static navigationOptions = {
    header: <HeaderTab title={"Face-to-Face"} />
  };

  constructor(props) {
    super(props);

    this.onChangeText = this.onChangeText.bind(this);
    this.national2 = this.updateRef.bind(this, "name2");
    this.national1 = this.updateRef.bind(this, "name1");

    this.state = {
      avatar1: "BR",
      name1: "Brazil",
      avatar2: "DE",
      name2: "Germany",
      match: 3,
      win: 2,
      draw: 0,
      lose: 1,
      goal: 4,
      goalLose: 7
    };
  }

  onChangeText(text) {
    ["avatar1", "name1", "avatar2", "name2"]
      .map(name => ({ name, ref: this[name] }))
      .filter(({ ref }) => ref && ref.isFocused())
      .forEach(({ name, ref }) => {
        this.setState({ [name]: text });

        let { name1, name2 } = this.state;
        let national = jsonData.data.national;
        let id1 = national.find(v => v.value === text);
        let id2;
        let id3;

        if (name === "name1") {
          id2 = national.find(v => v.value === name2);
          id3 = id1.id + "-" + id2.id;
          this.setState({ ["avatar1"]: id1.avatar });
        } else {
          id2 = national.find(v => v.value === name1);
          id3 = id2.id + "-" + id1.id;
          this.setState({ ["avatar2"]: id1.avatar });
        }

        const compare = jsonData.data.result.find(v => v.mapping === id3);

        this.setState({ ["match"]: compare ? compare.match : 0 });
        this.setState({ ["win"]: compare ? compare.win : 0 });
        this.setState({ ["draw"]: compare ? compare.draw : 0 });
        this.setState({ ["lose"]: compare ? compare.lose : 0 });
        this.setState({ ["goal"]: compare ? compare.goal : 0 });
        this.setState({ ["goalLose"]: compare ? compare.goalLose : 0 });
      });
  }

  updateRef(name, ref) {
    this[name] = ref;
  }

  render() {
    let {
      avatar1,
      name1,
      avatar2,
      name2,
      match,
      win,
      draw,
      lose,
      goal,
      goalLose
    } = this.state;

    return (
      <View style={styles.screen}>
        <View style={styles.container}>
          <Dropdown
            ref={this.national1}
            value={name1}
            onChangeText={this.onChangeText}
            label="National"
            data={jsonData.data.national}
          />
          <Dropdown
            ref={this.national2}
            value={name2}
            onChangeText={this.onChangeText}
            label="vs National"
            data={jsonData.data.national}
          />
        </View>
        <View style={{ flex: 1, flexDirection: "row", paddingTop: 60 }}>
          <Flag code={avatar1} size={64} style={{ marginLeft: 30 }} />
          <Text
            style={{
              fontSize: 18,
              textAlign: "center",
              flex: 1,
              color: "#dc143c",
              paddingTop: 20
            }}
          >
            VS
          </Text>
          <Flag code={avatar2} size={64} style={{ marginRight: 30 }} />
        </View>

        <View style={{ flex: 1, flexDirection: "row" }}>
          <View style={{ flex: 1 }}>
            <Text
              note
              style={{ fontSize: 16, paddingBottom: 5, alignSelf: "center" }}
            >
              Matches
            </Text>
            <Badge success style={{ alignSelf: "center" }}>
              <Text style={{ fontSize: 20, textAlign: "center" }}>{match}</Text>
            </Badge>
          </View>
          <View style={{ flex: 1 }}>
            <Text
              note
              style={{ fontSize: 16, paddingBottom: 5, alignSelf: "center" }}
            >
              Wins
            </Text>
            <Badge info style={{ alignSelf: "center" }}>
              <Text style={{ fontSize: 20, textAlign: "center" }}>{win}</Text>
            </Badge>
          </View>
          <View style={{ flex: 1 }}>
            <Text
              note
              style={{ fontSize: 16, paddingBottom: 5, alignSelf: "center" }}
            >
              Draws
            </Text>
            <Badge warning style={{ alignSelf: "center" }}>
              <Text style={{ fontSize: 20, textAlign: "center" }}>{draw}</Text>
            </Badge>
          </View>
          <View style={{ flex: 1 }}>
            <Text
              note
              style={{ fontSize: 16, paddingBottom: 5, alignSelf: "center" }}
            >
              Loses
            </Text>
            <Badge danger style={{ alignSelf: "center" }}>
              <Text style={{ fontSize: 20, textAlign: "center" }}>{lose}</Text>
            </Badge>
          </View>
        </View>

        <View style={{ flex: 1, flexDirection: "row" }}>
          <View style={{ flex: 1 }}>
            <Text
              note
              style={{ fontSize: 16, paddingBottom: 5, alignSelf: "center" }}
            >
              Goals+
            </Text>
            <Badge style={{ alignSelf: "center" }}>
              <Text style={{ fontSize: 20, textAlign: "center" }}>{goal}</Text>
            </Badge>
          </View>
          <View style={{ flex: 1 }}>
            <Text
              note
              style={{ fontSize: 16, paddingBottom: 5, alignSelf: "center" }}
            >
              Goals-
            </Text>
            <Badge primary style={{ alignSelf: "center" }}>
              <Text style={{ fontSize: 20, textAlign: "center" }}>
                {goalLose}
              </Text>
            </Badge>
          </View>
        </View>
        <AdsAdMob />
      </View>
    );
  }
}
const styles = {
  screen: {
    flex: 1,
    padding: 4,
    paddingTop: 15,
    backgroundColor: "#fff" //E8EAF6
  },

  container: {
    marginHorizontal: 4,
    marginVertical: 8,
    paddingHorizontal: 8
  },

  text: {
    textAlign: "center"
  }
};

const jsonData = require("../json/compareNational.json");

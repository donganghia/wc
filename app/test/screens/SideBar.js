import React from "react";
import { AppRegistry, Image, StatusBar, Platform } from "react-native";
import { Container, Content, Text, List, ListItem, Icon } from "native-base";
import styles from "./style";
const routes = [
  {
    name: "National",
    route: "National",
    icon: Platform.OS === "ios" ? "ios-medal" : "md-medal"
  },
  {
    name: "Player",
    route: "Player",
    icon: Platform.OS === "ios" ? "ios-shirt" : "md-shirt"
  }
];
export default class SideBar extends React.Component {
  render() {
    return (
      <Container>
        <Content>
          <List
            dataArray={routes}
            renderRow={data => {
              return (
                <ListItem
                  button
                  onPress={() => this.props.navigation.navigate(data.route)}
                >
                  <Icon name={data.icon} />
                  <Text style={styles.text}>{data.name}</Text>
                </ListItem>
              );
            }}
          />
        </Content>
      </Container>
    );
  }
}

import React, { Component } from "react";
import {
  Container,
  Content,
  Header,
  Left,
  List,
  ListItem,
  ScrollableTab,
  Tab,
  Tabs,
  Text,
  Title
} from "native-base";
import { StyleSheet, View } from "react-native";
import { Row, Table } from "react-native-table-component";
import Flag from "react-native-flags";
import AdsAdMob from "../components/AdsAdMob";
import   { database }  from "../api/firebase";
export default class DetailScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: `World cup ${navigation.state.params.year}`
  });

  constructor(props) {
      super(props);
      this.state = { 
        data: [], 
        id: props.navigation.state.params.id 
      };
  }

  componentDidMount() {
    //connect to a Firebase table
    var dbref = database.ref('getWorldcupInfo');   
 
    if(this.state.id == 21 && database.repo_.persistentConnection_.connected_)
    {
      dbref.on('value', (e) => {
        var rows = []; 
        if ( e && e.val() && e.val().map ) {
            e.val().map((v) => rows.push ( v ));
            this.setState({
              data: rows
            });
        } 
        else {
          this.getJsonData();
        }
      });
    }
    else
    {
      this.getJsonData();
    }
  }
  
  getJsonData() {
    const jsonData = require("../json/getWorldcupInfo.json");
    this.setState({
      data: jsonData.data
    });
  }

  renderTab(v) {
    const tableHead = [
      "",
      <Text style={styles.headerText}>Team</Text>,
      <Text style={styles.headerText}>M</Text>,
      <Text style={styles.headerText}>W</Text>,
      <Text style={styles.headerText}>D</Text>,
      <Text style={styles.headerText}>L</Text>,
      <Text style={styles.headerText}>G+</Text>,
      <Text style={styles.headerText}>G-</Text>,
      <Text style={styles.headerText}>P</Text>
    ];
    return (
      <Tab key={v.groupId} heading={v.groupName} style={{ flex: 1 }}>
        <Content style={styles.content}>
          <List>
            <ListItem itemDivider>
              <Text style={styles.titleText}>Matches</Text>
            </ListItem>
          </List>
          {v.match.map((s, k) => (
            <List key={k}>
              <ListItem style={{ height: 20 }}>
                <Text note style={{ fontSize: 12 }}>
                  {`${s.date} ${s.stadium}`}
                </Text>
              </ListItem>
              <ListItem
                style={{
                  flex: 1,
                  justifyContent: "center",
                  height: 40
                }}
              >
                <Text style={{ flex: 1.5 }}>
                  <Flag code={s.result[0].avatar} size={24} />
                  <Text style={{ fontSize: 15 }} >{s.result[0].name}</Text>
                </Text>
                <Text style={{ fontSize: 15, flex: 1 }}>
                  <Text style={{ backgroundColor: "#f4efef" }}>
                    {s.result[0].goal} - {s.result[1].goal}
                  </Text>
                </Text>
                <Text style={{ flex: 1.1 }}>
                  <Flag code={s.result[1].avatar} size={24} />
                  <Text style={{ fontSize: 15 }} >{s.result[1].name}</Text>
                </Text>
              </ListItem>
            </List>
          ))}
          <List style={{ paddingTop: 5 }}>
            <ListItem itemDivider>
              <Text style={styles.titleText}>Tables</Text>
            </ListItem>
          </List>
          <Table
            style={styles.table}
            borderStyle={{ borderWidth: 1, borderColor: "#c8e1ff" }}
          >
            <Row
              data={tableHead}
              style={styles.head}
              textStyle={styles.rowText}
              flexArr={[1, 3, 1, 1, 1, 1, 1, 1, 1]}
            />
            {v.rank.map((s, k) => (
              <Row
                key={k}
                data={[
                  <Flag style={{ alignItems: 'center' }} code={s.avatar ? s.avatar : null} size={32} />,
                  <Text style={styles.text}>{s.teamName}</Text>,
                  <Text style={styles.headerText}>
                    {s.wins + s.draws + s.losses}
                  </Text>,
                  <Text style={styles.headerText}>{s.wins}</Text>,
                  <Text style={styles.headerText}>{s.draws}</Text>,
                  <Text style={styles.headerText}>{s.losses}</Text>,
                  <Text style={styles.headerText}>{s.goalsFor}</Text>,
                  <Text style={styles.headerText}>{s.goalsAgainst}</Text>,
                  <Text style={styles.headerText}>{s.points}</Text>
                ]}
                style={styles.row}
                textStyle={styles.rowText}
                flexArr={[1, 3, 1, 1, 1, 1, 1, 1, 1]}
              />
            ))}
          </Table>
        </Content>
      </Tab>
    );
  }

  renderFinalStagesTab(v) {
    return (
      <Tab key={"finalStages"} heading={"Final Stages"} style={{ flex: 1 }}>
        <Content style={styles.content}>
          {v.map((s, k) => this.renderFinalStagesListItem(s, k))}
        </Content>
      </Tab>
    );
  }

  renderFinalStagesListItem(s, k) {
    const t = s.stageResult;
    return (
      <View>
        <List style={{ paddingTop: 5 }} key={k}>
          <ListItem key={s.stageName} itemDivider>
            <Text style={styles.titleText}>{s.stageName}</Text>
          </ListItem>
        </List>
        {t.map(u => (
          <List  style={{ paddingTop: 5 }} key={u.id}>
            <ListItem style={{ height: 20 }}>
              <Text
                note
                style={{ fontSize: 12 }}
              >{`${u.date} ${u.stadium}`}</Text>
            </ListItem>
            <ListItem style={{ flex: 1, height: 40 }}>
              <Text style={{ fontSize: 15, flex: 1.2 }}>
                <Flag code={u.result[0].avatar} size={24} />
                {u.result[0].name}
              </Text>
              <Text style={{ fontSize: 15, flex: 1 }}>
                <Text style={{ backgroundColor: "#f4efef" }}>
                  {u.result[0].goal} - {u.result[1].goal}
                </Text>
                <Text note>{u.extend != "" ? ` ${u.extend}` : null}</Text>
              </Text>
              <Text style={{ fontSize: 15, flex: 1 }}>
                <Flag code={u.result[1].avatar} size={24} style={{ flex: 1 }} />
                {u.result[1].name}
              </Text>
            </ListItem>
          </List>
        ))}
      </View>
    );
  }

  render() {
    const data = (this.state.data || []).find(v => v.id === this.state.id);
    return (
      <Container>
        {data ? (
          <Tabs style={{ flex: 1 }} renderTabBar={() => <ScrollableTab />}>
            {data.info.chart ? (
              data.info.chart.map(v => this.renderTab(v))
            ) : null}
            {data.info.finalStages ? (
              this.renderFinalStagesTab(data.info.finalStages)
            ) : null}
          </Tabs>
        ) : null}
        <AdsAdMob bannerSize={this.state.id == 21 ? "fullBanner" : "banner" } />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  head: { height: 36, backgroundColor: "#f1f8ff" },
  text: {
    fontSize: 15
  },
  headerText: {
    fontSize: 15,
    textAlign: "center"
  },
  rowText: {
    textAlign: "center"
  },
  row: { height: 36 },
  titleText: {
    fontSize: 16,
    fontWeight: "bold"
  },
  content: {
    flex: 1,
    paddingTop: 5,
    paddingBottom: 5,
    backgroundColor: "#fff"
  }
});



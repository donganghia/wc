import React, { Component } from "react";
import { Platform, StyleSheet, View } from "react-native";
import Flag from "react-native-flags";
import HeaderTab from "../components/HeaderTab";
import AdsAdMob from "../components/AdsAdMob";
import { Ionicons } from "react-native-vector-icons";
import {
  Body,
  Button,
  Container,
  Content,
  Icon,
  Left,
  List,
  ListItem,
  Right,
  Text
} from "native-base";

export default class TournamentScreen extends Component {
  static navigationOptions = {
    header: <HeaderTab title={"Tournaments"} />
  };
  
  componentDidMount() {
  }

  componentWillUnmount() {

  }

  renderRow(v) {
    const iconHome = Platform.OS === "ios" ? "ios-home" : "md-home";
    const iconChampion = Platform.OS === "ios" ? "ios-trophy" : "md-trophy";
    const iconArrow =
      Platform.OS === "ios" ? "ios-arrow-forward" : "md-arrow-forward";
    const { navigate } = this.props.navigation;

    return (
      <ListItem 
        style={{ height: 65 }} 
        onPress={() => {navigate("Detail", { id: v.id, year: v.year }); }} >
        <Flag code={v.avatar} size={48} />
        <Body>
          <Text style={{ fontSize: 15 }}>{v.year}</Text>
          <Text note style={{ fontSize: 14 }}>
            <Ionicons name={iconHome} size={14} />
            {` ${v.host}`}
          </Text>
          <Text note style={{ fontSize: 14 }}>
            <Ionicons name={iconChampion} size={14} />
            {` ${v.champion}`}
          </Text>
        </Body>
        <Right>
            <Ionicons name={iconArrow} />
        </Right>
      </ListItem>
    );
  }

  render() {
    return (
      <Container style={styles.container}>
        <Content>
          <List dataArray={jsonData.data} renderRow={v => this.renderRow(v)} />
        </Content> 
        <AdsAdMob />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 5,
    backgroundColor: "#fff"
  }
});

const jsonData = require("../json/findWorldcup.json");

import React from "react";
import { Platform, StyleSheet } from "react-native";
import Flag from "react-native-flags";
import { Ionicons } from "react-native-vector-icons";
import AdsAdMob from "../components/AdsAdMob";
import {
  Body,
  Container,
  Content,
  Left,
  List,
  ListItem,
  Text,
  View
} from "native-base";
import HeaderTab from "../components/HeaderTab";

export default class StatisticScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    header: <HeaderTab navigation={navigation} title={"National Ranks"} />
  });

  renderRow(v) {
    const iconChampion = Platform.OS === "ios" ? "ios-trophy" : "md-trophy";
    return (
      <ListItem style={{ height: 65 }} avatar>
        <Left>
          <Flag code={v.avatar} size={48} />
        </Left>
        <Body>
          <View style={{ flex: 1, flexDirection: "row" }}>
            <Text style={{ flex: 3.2 }}>{v.name}</Text>
            <Text style={{ color: "steelblue", flex: 1 }}>{v.winners}</Text>
            <Text style={{ color: "steelblue", flex: 1 }}>{v.runnersUp}</Text>
            <Text style={{ color: "steelblue", flex: 1 }}>{v.third}</Text>
            <Text style={{ color: "steelblue", flex: 1 }}>{v.fourth}</Text>
          </View>
          <Text note style={{ fontSize: 13 }}>
            {v.winners ? <Ionicons name={iconChampion} size={13} /> : null}
            {v.winnersYears ? v.winnersYears.map(v => " " + v) : ""}
          </Text>
        </Body>
      </ListItem>
    );
  }

  render() {
    return (
      <Container style={styles.container}>
        <Content>
          <List>
            <ListItem>
              <Body>
                <View style={{ flex: 1, flexDirection: "row" }}>
                  <Text style={{ flex: 5 }}>National</Text>
                  <Text style={{ flex: 1 }}>1st</Text>
                  <Text style={{ flex: 1 }}>2nd</Text>
                  <Text style={{ flex: 1 }}>3rd</Text>
                  <Text style={{ flex: 1 }}>4th</Text>
                </View>
              </Body>
            </ListItem>
          </List>
          <List dataArray={jsonData.data} renderRow={v => this.renderRow(v)} />
        </Content>
        <AdsAdMob />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 5,
    backgroundColor: "#fff"
  },
  name: {
    width: 50
  },
  list: {
    height: 50
  }
});

const jsonData = require("../json/statisticNational.json");

import React from "react";
import { StyleSheet } from "react-native";
import Flag from "react-native-flags";
import { Ionicons } from "react-native-vector-icons";
import {
  Body,
  Button,
  Container,
  Content,
  Header,
  Icon,
  Left,
  List,
  ListItem,
  Text,
  Title,
  View
} from "native-base";
import HeaderTab from "../components/HeaderTab";

export default class PlayerScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    header: <HeaderTab navigation={navigation} title={"Top Scorers"} />
  });

  renderRow(v) {
    return (
      <ListItem>
        <Body>
          <View style={{ flex: 1, flexDirection: "row" }}>
            <Text style={{ flex: 1 }}>{v.id}</Text>
            <Text style={{ color: "steelblue", flex: 3 }}>{v.name}</Text>
            <Flag style={{ flex: 0.4 }} code={v.avatar} size={24} />
            <Text  style={{ flex: 1.6 }}>{v.country}</Text>
            <Text style={{ color: "steelblue", flex: 1 }}>{v.score}</Text>
          </View>
        </Body>
      </ListItem>
    );
  }

  render() {
    return (
      <Container style={styles.container}>
        <Content>
          <List>
            <ListItem>
              <Body>
                <View style={{ flex: 1, flexDirection: "row" }}>
                  <Text style={{ flex: 1 }}>Rank</Text>
                  <Text style={{ flex: 3 }}>Player</Text>
                  <Text style={{ flex: 2 }}>Country</Text>
                  <Text style={{ flex: 1 }}>Goal</Text>
                </View>
              </Body>
            </ListItem>
          </List>
          <List dataArray={jsonData.data} renderRow={v => this.renderRow(v)} />
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 5,
    backgroundColor: "#fff"
  },
  name: {
    width: 50
  },
  list: {
    height: 50
  }
});

const jsonData = require("../json/statisticPlayer.json");

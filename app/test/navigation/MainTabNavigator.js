import React from "react";
import { Platform } from "react-native";
import { Ionicons } from "react-native-vector-icons";
import {
  TabNavigator,
  TabBarBottom,
  StackNavigator,
  DrawerNavigator
} from "react-navigation";

import Colors from "../constants/Colors";

import StatisticScreen from "../screens/Statistic";
import TournamentScreen from "../screens/Tournament";
import DetailScreen from "../screens/Detail";
import CompareScreen from "../screens/Compare";
import PlayerScreen from "../screens/Player";
import SideBar from "../screens/SideBar";

const StatisticDrawer = DrawerNavigator(
  {
    National: { screen: StatisticScreen },
    Player: { screen: PlayerScreen }
  },
  {
    contentComponent: props => <SideBar {...props} />,
    drawerWidth: 180
  }
);

const TournamentStack = StackNavigator({
  Tournament: {
    screen: TournamentScreen
  },
  Detail: {
    screen: DetailScreen
  }
});

const TabNav = TabNavigator(
  {
    Tournament: {
      screen: TournamentStack,
      navigationOptions: {
        title: "Tournaments",
        header: null
      }
    },
    Statistic: {
      screen: StatisticDrawer
    },
    Compare: {
      screen: CompareScreen
    }
  },
  {
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused }) => {
        const { routeName } = navigation.state;
        let iconName;
        switch (routeName) {
          case "Tournament":
            iconName =
              Platform.OS === "ios"
                ? `ios-football${focused ? "" : "-outline"}`
                : "md-football";
            break;
          case "Statistic":
            iconName =
              Platform.OS === "ios"
                ? `ios-stats${focused ? "" : "-outline"}`
                : "md-stats";
            break;
          case "Compare":
            iconName =
              Platform.OS === "ios"
                ? `ios-sync${focused ? "" : "-outline"}`
                : "md-sync";
        }
        return (
          <Ionicons
            name={iconName}
            size={30}
            style={{ marginBottom: -3 }}
            color={focused ? Colors.tabIconSelected : Colors.tabIconDefault}
          />
        );
      }
    }),
    tabBarComponent: TabBarBottom,
    tabBarPosition: "bottom",
    animationEnabled: false,
    swipeEnabled: false
  }
);

export default TabNav;

import React from "react";
import PropTypes from "prop-types"; // ES6
import { Body, Header, Left, Title, Right, Button, Icon } from "native-base";

export default class HeaderTab extends React.Component {
  static propTypes = {
    title: PropTypes.string,
    navigation: PropTypes.object.isRequired
  };

  render() {
    return (
      <Header>
        <Left>
          {this.props.navigation ? (
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("DrawerOpen")}
            >
              <Icon name="menu" />
            </Button>
          ) : null}
        </Left>
        <Body>
          <Title>{this.props.title}</Title>
        </Body>
        <Right />
      </Header>
    );
  }
}

import React from "react";
import { Platform } from "react-native";
import PropTypes from "prop-types"; // ES6
import { AdMobBanner } from 'react-native-admob';

export default class AdsAdMob extends React.Component {
  static propTypes = {
    bannerSize: PropTypes.string
  };

  render() {
    const bannerSize = this.props.bannerSize ? this.props.bannerSize : "banner";
    return (
        <AdMobBanner
            bannerSize={bannerSize}
            adUnitID={adsUnitID}
            testDeviceID="EMULATOR"
        />
    );
  }
}
const adsUnitID = Platform.OS === "ios" ? "ca-app-pub-3778562018876168/8871710017" : "ca-app-pub-3778562018876168/7696357588";


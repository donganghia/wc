import React from 'react';
import {Platform, StyleSheet, StatusBar, View} from 'react-native';
import {AppLoading, Asset, Font} from 'expo';
import MainNavigator from './navigation/MainNavigator';

export default class App extends React.Component {
  state = {
    assetsAreLoaded: false
  };

  componentWillMount() {
    this._loadAssetsAsync();
  }

  render() {
    if (!this.state.assetsAreLoaded) {
      return <AppLoading/>;
    } else {
      return (
        <View style={styles.container}>
          {Platform.OS === 'ios' && <StatusBar barStyle="default"/>}
          {Platform.OS === 'android' && <View style={styles.statusBarUnderlay}/>}
          <MainNavigator/>
        </View>
      );
    }
  }

  async _loadAssetsAsync() {
    try {
      await Promise.all([Font.loadAsync([// This is the font that we are using for our tab bar Ionicons.font, We include
          // SpaceMono because we use it in HomeScreen.js. Feel free to remove this if you
          // are not using it in your app { 'cutive-mono':
          // require('./assets/fonts/CutiveMono-Regular.ttf') },
          {
            Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
          }
        ])]);
    } catch (e) {
      // In this case, you might want to report the error to your error reporting
      // service, for example Sentry
      console.warn('There was an error caching assets (see: App.js), perhaps due to a network timeou' +
          't, so we skipped caching. Reload the app to try again.');
      console.log(e);
    } finally {
      this.setState({assetsAreLoaded: true});
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    //alignItems: 'center', justifyContent: 'center',
  },
  statusBarUnderlay: {
    height: 24,
    backgroundColor: 'rgba(0,0,0,0.2)'
  }
});

import React, { Component } from "react";
import {
  Body,
  Button,
  Container,
  Content,
  Header,
  Left,
  Right,
  List,
  ListItem,
  ScrollableTab,
  Tab,
  Tabs,
  Text,
  Title
} from "native-base";
import { Platform, StyleSheet, View } from "react-native";
import { Row, Table } from "react-native-table-component";
import { Ionicons } from "react-native-vector-icons";

export default class DetailScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: `${navigation.state.params.title}`
  });

  render() {
    const { params } = this.props.navigation.state;
    const iconArrow =
      Platform.OS === "ios" ? "ios-arrow-forward" : "md-arrow-forward";
    const { navigate } = this.props.navigation;
    //const data = (jsonData.data || []).find(v => v.id === params.id);
   return (
      <Container style={styles.container}>
        <Content>
          <List>
            <ListItem 
              onPress={() => {
                navigate("Lesson", { id: "A1_katsudou_1", title: "だい1か"});
              }}
            >
              <Body>
                <Text>だい1か</Text>
              </Body>
              <Right>
                <Ionicons name={iconArrow} />
              </Right>
            </ListItem>
            <ListItem 
              onPress={() => {
                navigate("Lesson", { id: "A1_katsudou_2", title: "だい2か"});
              }}
            >
              <Body>
                <Text>だい2か</Text>
              </Body>
              <Right>
                <Ionicons name={iconArrow} />
              </Right>
            </ListItem>
          </List>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 5,
    backgroundColor: "#fff"
  }
});

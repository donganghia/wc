import React, {Component} from "react";
import {Platform, StyleSheet} from "react-native";
import {
  Body,
  Button,
  Container,
  Content,
  Icon,
  Left,
  List,
  ListItem,
  Right,
  Text
} from "native-base";
import {Ionicons} from "react-native-vector-icons";
import HeaderTab from "../components/HeaderTab";
export default class HomeScreen extends Component {
  render() {
    const iconArrow = Platform.OS === "ios"
      ? "ios-arrow-forward"
      : "md-arrow-forward";
    const {navigate} = this.props.navigation;

    return (
      <Container style={styles.container}>
        <HeaderTab title={"まるごと"}></HeaderTab>
        <Content>
          <List>
            <ListItem itemDivider>
              <Text>入門（A1）</Text>
            </ListItem>
            <ListItem
              onPress={() => {
              navigate("Detail", {
                id: "A1_katsudou",
                title: "入門（A1）かつどう"
              });
            }}>
              <Body>
                <Text>かつどう</Text>
              </Body>
              <Right>
                <Ionicons name={iconArrow}/>
              </Right>
            </ListItem>
            <ListItem
              onPress={() => {
              navigate("Detail", {
                id: "A1_rikai",
                title: "入門（A1）りかい"
              });
            }}>
              <Body>
                <Text>りかい</Text>
              </Body>
              <Right>
                <Ionicons name={iconArrow}/>
              </Right>
            </ListItem>
          </List>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 5,
    backgroundColor: "#fff"
  }
});

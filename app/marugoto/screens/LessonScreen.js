import React, {Component} from "react";
import {
  Body,
  Button,
  Container,
  Content,
  Header,
  Left,
  Right,
  List,
  ListItem,
  ScrollableTab,
  Tab,
  Tabs,
  Text,
  Title
} from "native-base";
import {Platform, StyleSheet, View} from "react-native";
import {Row, Table} from "react-native-table-component";
import {Ionicons} from "react-native-vector-icons";
import DetailScreen from "../screens/DetailScreen";
//import AudioRecording from "../components/AudioRecording";
//import Sound from "react-native-sound";
//import MusicControl from 'react-native-music-control';
import MusicPlayerService, { Track, Events, RepeatModes } from 'react-native-music-player-service';
import Expo, {Asset, Audio, FileSystem, Font, Permissions} from "expo";

export default class LessonScreen extends Component {
  static navigationOptions = ({navigation}) => ({title: `${navigation.state.params.title}`});

  constructor(props) {
    super(props);
    // this.state = {
    //   isPlaying: false
    // };

    // this.loadAudio = this
    //   .loadAudio
    //   .bind(this);
    // this.playSound = this
    //   .playSound
    //   .bind(this);
  }

  componentDidMount() {
    //this.loadAudio();
  }

  componentWillUnmount() {
    // this
    //   .soundObject
    //   .stopAsync();
  }

  async loadAudio() {
    this.soundObject = new Audio.Sound();
    try {
      await this
        .soundObject
        .loadAsync(require('../assets/sounds/001_A1_katsudoo_title.mp3'));
    } catch (e) {
      console.log('ERROR Loading Audio', e);
    }
  }

  playSound() {
    var musicPlayerService = new MusicPlayerService(true, setNowPlayingConfig);

    /* Initialization */
    musicPlayerService.addEventListener(Events.Play, track => _event(Events.Play, track));
    musicPlayerService.addEventListener(Events.Pause, track => _event(Events.Pause, track));
    musicPlayerService.addEventListener(Events.Next, track => _event(Events.Next, track));
    musicPlayerService.addEventListener(Events.Previous, track => _event(Events.Previous, track));
    musicPlayerService.addEventListener(Events.EndReached, track => _event(Events.EndReached, track));

    /* Setting up the queue */
    var songsInformation = [
        {
            id: "1",
            path: "../assets/sounds/001_A1_katsudoo_title.mp3",
            title: "track_1",
            album: "some album",
            artist: "some artist",
            genre: "some genre",
            duration: 2260,
            artwork: "https://i.imgur.com/e1cpwdo.png"
        }
    ]
    var tracks = songsInformation.map(s => {
        return new Track({id: s.id, path: s.path, additionalInfo: s});
    })

    musicPlayerService.setQueue(tracks)
      .then(returnedQueue => {
          console.log('Queue has been set');
          return musicPlayerService.togglePlayPause();
      })
      .then(() => {
        console.log('Play or pause has been toggled');
      });

    musicPlayerService.playNext();
    // this.setState({
    //   isPlaying: !this.state.isPlaying
    // }, () => (this.state.isPlaying
    //   ? this.soundObject.playAsync()
    //   : this.soundObject.stopAsync()));
  }

  // playMusic() {
  //   MusicControl.setNowPlaying({
  //     title: 'Billie Jean',
  //     artwork: 'https://i.imgur.com/e1cpwdo.png', // URL or RN's image require()
  //     artist: 'Michael Jackson',
  //     album: 'Thriller',
  //     genre: 'Post-disco, Rhythm and Blues, Funk, Dance-pop',
  //     duration: 294, // (Seconds)
  //     description: '', // Android Only
  //     color: 0xFFFFFF, // Notification Color - Android Only
  //     date: '1983-01-02T00:00:00Z', // Release Date (RFC 3339) - Android Only
  //     rating: 84, // Android Only (Boolean or Number depending on the type)
  //     notificationIcon: 'my_custom_icon' // Android Only (String), Android Drawable resource name for a custom notification icon
  //   })
  // }

  render() {
    const {params} = this.props.navigation.state;
    const iconPlay = Platform.OS === "ios"
      ? "ios-play"
      : "md-play";
    return (
      <Container style={styles.container}>
        <Content>
          <List>
            <ListItem>
              <Body>
                <Text>A1_katsudoo_title</Text>
              </Body>
              <Right>
                <Button transparent onPress={this.playSound}>
                  <Ionicons name={iconPlay}/>
                </Button>
              </Right>
            </ListItem>
            <ListItem onPress={() => {
              this.playMusic();
            }}>
              <Body>
                <Text>A1_katsudoo_1_1_2</Text>
              </Body>
              <Right>
                <Ionicons name={iconPlay}/>
              </Right>
            </ListItem>
          </List>
          {/* <AudioRecording/> */}
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 5,
    backgroundColor: "#fff"
  }
});


const _event = (event, track) => { 
    console.log(event.toString() + ' has been raised with ' + track.toString());
}

const setNowPlayingConfig = {
  color: 0x2E2E2E,
  notificationIcon: 'my_custom_icon'
}
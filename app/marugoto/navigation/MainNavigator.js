import React, { Component } from "react";
import { StackNavigator } from "react-navigation";
import HomeScreen from "../screens/HomeScreen";
import DetailScreen from "../screens/DetailScreen";
import LessonScreen from "../screens/LessonScreen";

const MarugotoApp = StackNavigator(
	{
		Home: { screen: HomeScreen },
		Detail: { screen: DetailScreen },
		Lesson: { screen: LessonScreen },
	},
	{
		initialRouteName: "Home"
	}
);

export default class MainNavigator extends Component {
  render() {
    return <MarugotoApp />;
  }
}

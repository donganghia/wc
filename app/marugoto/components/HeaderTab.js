import React from "react";
import PropTypes from "prop-types"; // ES6
import { Body, Header, Left, Title, Right, Button, Icon } from "native-base";

export default class HeaderTab extends React.Component {
  static propTypes = {
    title: PropTypes.string.isRequired
  };

  render() {
    return (
      <Header>
        <Body>
          <Title>{this.props.title}</Title>
        </Body>
        <Right />
      </Header>
    );
  }
}
